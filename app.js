const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 4008;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://mnballesca0229:09283542019@s36.yo0msap.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open",() => console.log('Now connected to the database!'));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));