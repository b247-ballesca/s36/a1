const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController")

router.get("/", (req, res) => {
	taskController.getAllTask().then(resultFromController => res.send(
		resultFromController))
});

router.post("/", (req, res) => {
	taskController.creatTask(req.body).then(resultFromController => res.send(
		resultFromController))
});


router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(
		resultFromController));
})


router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router;